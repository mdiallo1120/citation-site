-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 23, 2022 at 11:02 AM
-- Server version: 8.0.28-0ubuntu0.20.04.3
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `citation`
--

-- --------------------------------------------------------

--
-- Table structure for table `citations`
--

CREATE TABLE `citations` (
  `id` bigint UNSIGNED NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `auteur_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `citations`
--

INSERT INTO `citations` (`id`, `content`, `auteur_id`, `created_at`, `updated_at`) VALUES
(1, 'Est qui quia impedit similique enim voluptatem dolor. Et consequatur molestiae qui in a voluptatum est. Quia ipsum atque atque dignissimos deserunt repellendus provident.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(2, 'Sunt laboriosam id eum. Voluptatem officiis quaerat consequatur quia quo. Ut est unde quisquam in ea officia animi dolor. Omnis ratione vel optio animi adipisci eum.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(3, 'Sit optio occaecati facere mollitia dolorum voluptas quis. Delectus aperiam quidem perferendis pariatur. Earum magnam cupiditate sunt.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(4, 'Eveniet commodi sed voluptatem non voluptas ex. Praesentium ut quae et ut iure sapiente itaque quae. Incidunt voluptas praesentium minima reprehenderit ab nobis et.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(5, 'Hic accusantium soluta eveniet. Nam minus ipsam explicabo. Voluptatem voluptatem eos eaque voluptas quos qui reiciendis. Atque laboriosam deserunt quis.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(6, 'Necessitatibus corrupti et perferendis enim labore. Hic dolores voluptas error sequi et unde placeat beatae. Voluptates sint quia nulla natus et assumenda consequuntur voluptas. Ab qui eius quis repudiandae voluptas amet repellat est.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(7, 'Vel sequi eligendi quo consequuntur laudantium et fuga modi. Maiores unde perspiciatis minus. Aut a laboriosam sit soluta.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(8, 'Rerum voluptatem odit beatae error voluptatem numquam consectetur. Suscipit nostrum quos accusamus tenetur maiores veritatis repellendus. Libero non quo quia sed nulla consectetur eligendi itaque.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(9, 'Voluptas aspernatur molestias magnam repellat aperiam. Eos facilis aspernatur architecto voluptates. Ea ut qui exercitationem ut eius similique totam. Quis enim eos eaque magnam ut.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(10, 'Minima itaque explicabo reprehenderit. Nobis laboriosam error expedita facilis a aut eum expedita. Velit consequatur qui sequi eum exercitationem voluptates nobis.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(11, 'Voluptas nesciunt voluptas dolorem itaque quo vero. Praesentium sequi et officiis eligendi accusamus est minus. Temporibus qui enim ea eos autem. Aut qui consequatur voluptates earum omnis dolores.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47'),
(12, 'Neque placeat officia ut omnis odit quia et. Consequatur ut voluptatem aliquid facilis id deleniti sapiente. Id vero alias ut fuga et ut et inventore. Reiciendis fugiat sit aliquid suscipit cupiditate. Excepturi dolores et cupiditate quibusdam deserunt et.', 1, '2022-02-23 08:46:47', '2022-02-23 08:46:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `citations`
--
ALTER TABLE `citations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `citations`
--
ALTER TABLE `citations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
