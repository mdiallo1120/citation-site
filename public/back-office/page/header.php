<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../public/style.css">
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <title>Admin</title>
</head>
<body style="min-height: 100vh;">
    <nav class="main-nav" >       
     <div class="logo" >Nav</div>        
     <ul class="nav-links" >                   
       <li><a href="/back-office/index.php" >Citations</a></li>            
       <li><a href="/back-office/index.php?page=auteurs">Auteurs</a></li>                 
    </ul>       
    <div class="burger" onclick="nav()">            

        <div class="line line1"></div>           
         <div class="line line2"></div>            
         <div class="line line3"></div>       
        </div>    
    </nav>
