




<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);




require('../page/header.php');
 ?>


<?php 
$page = isset($_GET['page']) ? $_GET['page'] : 'home';

switch($page) {
	case 'citations':
		include('../component/citations.php');
		break;

	case 'auteur':
		include('../component/autheur.php');
		break;

	case 'back-office':
		include ('../back-office/login-back.php');
		break;

	default:
		include('../component/home.php');
		break;
}


?>


<?php require('../page/footer.php') ?>



<!-- session_start();
    if(!isset($_SESSION['Alireza'])){
        header("Location: index.php");
    } -->