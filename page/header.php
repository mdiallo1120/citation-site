<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <title>Document</title>
</head>
<body>
    <nav class="main-nav" >       
     <div class="logo" >Nav</div>        
     <ul class="nav-links" >           
      <li><a href="/index.php?page=home">Home</a></li>           
       <li><a href="/index.php?page=citations" >Citations</a></li>            
       <li><a href="/index.php?page=auteur">Auteurs</a></li>           
        <li><a href="#">Contact</a></li>       
    </ul>       
    <div class="burger" onclick="nav()">            

        <div class="line line1"></div>           
         <div class="line line2"></div>            
         <div class="line line3"></div>       
        </div>    
    </nav>
